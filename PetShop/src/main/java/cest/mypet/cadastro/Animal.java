package cest.mypet.cadastro;

import cest.mypet.cadastro.TipoAnimal;

public class Animal {
	private String nome;
	private int idade;
	private TipoAnimal tipo;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public TipoAnimal getTipo() {
		return tipo;
	}
	public void setTipo(TipoAnimal tipo) {
		this.tipo = tipo;
	}
	
	
}
