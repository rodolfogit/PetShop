package cest.mypet.teste.loc;

import cest.mypet.cadastro.loc.Cidade;
import cest.mypet.cadastro.loc.UF;

public class TesteLoc {

	public static void main(String[] args) {
		Cidade c = new Cidade();
		c.setNome("Sao Luis");
		
		UF uf = new UF();
		uf.setCod("MA");
		uf.setDescricao("Maranhão");
		
		c.setUf(uf); 
		
		System.out.println("COD UF: " + c.getUf().getCod());
		System.out.println("UF: " + c.getUf().getDescricao());
		System.out.println("Cidade: " + c.getNome());
				

	}

}
