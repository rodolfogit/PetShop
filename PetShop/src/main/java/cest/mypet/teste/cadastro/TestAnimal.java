package cest.mypet.teste.cadastro;

import cest.mypet.cadastro.Animal;
import cest.mypet.cadastro.TipoAnimal;

public class TestAnimal {

	public static void main(String[] args) {
		Animal a1 = new Animal();
		a1.setNome("Lulu");
		a1.setIdade(2);
			
		TipoAnimal tipo = new TipoAnimal();
		tipo.setCod(1);
		tipo.setDescricao("poodle");
		
		System.out.println("Nome: " + a1.getNome());
		System.out.println("Tipo: " + tipo.getDescricao());
		System.out.println("Cod Animal: " + tipo.getCod());
		System.out.println("Idade: " + a1.getIdade());
		

	}

}
